# Dir-Flattener

Simple cli tool for flattening a directory.

# Wdym??
E.g.: You're downloading a zip archive from an EDI archive somewhere. Now
imagine in that zip archive are 20 different folders containing a single file
each. It's a pain moving every file into the root directory, especially under
the toy-OS Windows.

This tool is supposed to help it :)

# Usage
dirflattener {-d,--directory} <DIRECTORY>
