use clap::Parser;
use std::fs;
use std::path::Path;
use walkdir::WalkDir;
use colored::Colorize;


/// simple cli tool to flatten directories
#[derive(Parser, Debug)]
#[command(author, version, about, long_about)]
struct Args {
    /// Directory to flatten
    #[arg(short, long)]
    directory: String,
}

fn main() {
    let args = Args::parse();

    let dir = args.directory;

    for entry in WalkDir::new(&dir)
            .into_iter()
            .filter_map(Result::ok)
            .filter(|x| !x.file_type().is_dir()) {
        let file_name = entry.file_name().to_string_lossy().to_string();
        let new_path = Path::new(&dir).join(&file_name);

        if let Err(err) = fs::rename(entry.path(), &new_path) {
            panic!("Error moving {:?}: {}", entry.path(), err);
        }
        println!("{} {:?}{}", "Moving".yellow(), entry.path(), "...".yellow());
    }
    println!("{}", "Finished!".green());
}
